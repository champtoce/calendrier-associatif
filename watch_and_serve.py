#!/usr/bin/env python3
# Script Dependencies:
#    livereload
import webbrowser
from livereload import Server

HOST = "127.0.0.1"  # "0.0.0.0"
PORT = 5555

server = Server()
server.watch("calendrier-intramuros.html")
webbrowser.open(f"http://localhost:{PORT}/calendrier-intramuros.html")
server.serve(host=HOST, port=PORT)
