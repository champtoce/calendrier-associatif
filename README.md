Sources du site <https://champtoce.frama.io/calendrier-associatif/>

Cette page a été conçue à partir :

* de l'[API IntraMuros](https://appli-intramuros.fr/widget-et-api/)
* de la lib Javascript [FullCalendar](https://fullcalendar.io)
* de [Micromodal.js](https://micromodal.vercel.app/)

Projets FLOSS akternatifs :
* <https://simonrepp.com/feber/>


## Alternative : diaporama Intramuros
<https://widget.intramuros.org/diapo.html?commune=669&vitesse=5>


## Améliorations possibles

### Afficher les anciens événements
L'API du widget ne semble pas permettre d'afficher les anciens événements.

Le scrapping <https://appli-intramuros.com/agenda/eventproxy/> le permettrait par contre.

### Inclure des données structurées LD+JSON
Preview:
```html
<script type="application/ld+json">
{
  "@context":"http://schema.org",
  "@type": "Event",
  "eventStatus": "https://schema.org/EventScheduled",
  "name" : "...",
  "description": "...",
  "startDate": "2023-10-30T10:00:00+0100",
  "endDate" : "2023-10-30T11:30:00+0100",
  "url": "...",
  "image": {"@type":"ImageObject", ... },
  "organizer": { ... },
  "location": [{ ... }],
  "geo": { "@type": "GeoCoordinates", ...},
  "performer" : { ... }
}
</script>
```

Ainsi, ces événements apparaitront dans le moteur de recherche Google.

[Documentation Google sur les données structurées LD+JSON](https://developers.google.com/search/docs/appearance/structured-data/intro-structured-data?hl=fr)

### Afficher la /publication/ IntraMuros dans la modale ?
Ex: <https://www.intramuros.org/publication/agenda/283593>
